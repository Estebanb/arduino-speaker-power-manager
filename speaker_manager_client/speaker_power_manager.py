#!/usr/bin/env python3
import serial
import time
import argparse


class SpeakerPowerManager():
    def __enter__(self, device='/dev/ttyUSB0', baudrate=9600):
        self.serial = serial.Serial(device, baudrate)
        self.__flush_serial()
        return self

    def speaker_set(self, speaker, status):
        self.serial.write(str.encode('TURN {} {}'.format(status, speaker)))
        time.sleep(2)
        assert '{} {}'.format(speaker, status) in self.read_output_from_serial() 

    def speaker_get(self, speaker):
        self.serial.write(str.encode('{} STATUS'.format(speaker)))
        time.sleep(2)
        return self.read_output_from_serial()

    def read_output_from_serial(self):
        return self.serial.read_all().decode()

    def __flush_serial(self):
        self.serial.read_all()

    def __exit__(self, type, value, traceback):
        self.serial.close()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--set", help="Status to set ON or OFF")
    parser.add_argument("-g", "--get", action="store_true", help="Get state from speakers")
    parser.add_argument("-l", "--speaker_list", nargs='+', help="Set state of speakers")
    args = parser.parse_args()

    with SpeakerPowerManager() as manager:
        if args.set:
            for speaker in args.speaker_list:
                manager.speaker_set(speaker, args.set)

        if args.get:
            for speaker in args.speaker_list:
                print(manager.speaker_get(speaker))


if __name__ == "__main__":
    main()
