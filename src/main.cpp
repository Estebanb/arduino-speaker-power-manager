#include <Arduino.h>

#define BASS 2
#define MID 3

void setup() {
    pinMode(BASS, OUTPUT);
    pinMode(MID, OUTPUT);
    Serial.begin(9600);
    Serial.println("Speaker power manager booting");
}

void loop() {

  if ( Serial.available() > 0) {

    String input = Serial.readString();

    if(input ==  (String)"TURN ON BASS"){
          digitalWrite(BASS, HIGH);
          Serial.print( "BASS ON \n");
    }
    else if(input ==  (String)"TURN OFF BASS"){
          digitalWrite(BASS, LOW);
          Serial.print( "BASS OFF \n");
    }
    else if(input ==  (String)"TURN ON MID"){
          digitalWrite(MID, HIGH);
          Serial.print( "MID ON \n");
    }
    else if(input ==  (String)"TURN OFF MID"){
          digitalWrite(MID, LOW);
          Serial.print( "MID OFF \n");
    }
    else if(input ==  (String)"MID STATUS"){
          if(digitalRead(MID)) Serial.print( "MID status: ON \n");
          else Serial.print("MID status: OFF \n");
    }
    else if(input ==  (String)"BASS STATUS"){
          if(digitalRead(BASS)) Serial.print( "BASS status: ON \n");
          else Serial.print("BASS status: OFF \n");
    }
    else {
      Serial.print("Commant invalid:");
      Serial.print(input);
      Serial.print("\n");
    }
    /*

       case MID:
          digitalWrite(3, !digitalRead(3));
          if(digitalRead(3)) Serial.print( "MID ON \n");
          else Serial.print( "MID OFF \n");
          break;  optional */




  }
}
