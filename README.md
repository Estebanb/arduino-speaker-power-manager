# Problem
I love music and I have a great speakers. But I hate turn ON and turn OFF my speakers when PC turns on.

# Solution
An arduino nano with 2 relay(BASS and MID) connected with USB to my computer.

Some code in C and python, managed with systemctl.
# Tools
Code for arduino its builded with platformio ide, http://docs.platformio.org/en/latest/.

Code for PC was made in python3 and serial communication with pySerial.

# Example:
```
speaker_power_manager.py -s OFF -l BASS
speaker_power_manager.py -s OFF -l BASS MID
speaker_power_manager.py -g -l BASS MID
```
# Help:
```
usage: speaker_power_manager.py [-h] [-s SET] [-g]
                                [-l SPEAKER_LIST [SPEAKER_LIST ...]]
```

# Arduino nano pinout
http://www.pighixxx.com/test/pinouts/boards/nano.pdf